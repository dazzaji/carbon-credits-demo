import m from 'mithril'

export const functionComponent = (view: () => m.Vnode<any, any>) => ({ view })
export const hero = (title: string, subtitle: string, page: string) => 
    m('div', [
        m('section', {class: 'hero'}, m('div', {class: 'hero-body'}, m('div', {class: 'container'}, [
            m('h1', {class: 'title'}, title),
            m('h2', {class: 'subtitle'}, subtitle),
        ]))),
        m('div', {class: `is-${page}-hl thicc-rule`}),
    ])

export type Page = 'acr' | 'project-owner' | 'verifier' | 'public' | 'settings'
export const tabs = ['acr', 'project-owner', 'verifier', 'public', 'spacer', 'settings']
export const icons: {[p: string]: string} = {
    'acr': 'fa-leaf',
    'project-owner': 'fa-tree',
    'verifier': 'fa-clipboard-check',
    'public': 'fa-user-friends',
    'settings': 'fa-cog'
}
export const getUrl = (path: string) => `${window.location.origin}/${path}`
export const users = {
    [getUrl('data/entity/acr')]: 'acr',
    [getUrl('data/entity/project-owner')]: 'project-owner',
    [getUrl('data/entity/acr')]: 'acr',
    [getUrl('data/entity/acr')]: 'acr',

}