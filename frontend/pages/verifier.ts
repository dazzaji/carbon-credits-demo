import m from 'mithril'
import { hero } from '../util'

export interface Attrs {}
interface State {}

export default {
    view(vnode) {
        return m('div', [
            hero('VERIFIER', `You're an accredited carbon verifier`, 'verifier')
        ])
    }
} as m.Component<Attrs, State>