import m from 'mithril'
import { hero } from '../util'

export interface Attrs {}
interface State {}

export default {
    view(vnode) {
        return m('div', [
            hero('MEMBER OF THE PUBLIC', `You're a member of the public`, 'public')
        ])
    }
} as m.Component<Attrs, State>