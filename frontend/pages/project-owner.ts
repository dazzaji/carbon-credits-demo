import m from 'mithril'
import { hero } from '../util'

export interface Attrs {}
interface State {}

export default {
    view(vnode) {
        return m('div', [
            hero('PROJECT OWNER', `You're the owner of the project, and you want credit for planting trees`, 'project-owner'),
        ])
    }
} as m.Component<Attrs, State>