import m from 'mithril'
import { hero } from '../util'

export interface Attrs {}
interface State {}

export default {
    view(vnode) {
        return m('div', [
            hero('SETTINGS', `Demo Settings`, 'settings')
        ])
    }
} as m.Component<Attrs, State>