import * as jsonld from 'jsonld/dist/jsonld.min'
import * as jsig from 'jsonld-signatures/dist/jsonld-signatures.min'
jsig.use('jsonld', jsonld)
import { getUrl } from '../util'

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!! THIS CODE HAS NOT BEEN AUDITED AND IS NOT SUITABLE FOR USE IN PRODUCTION! !!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

import m from 'mithril'

export type VerifiableClaim = {
    data: jsonld.JsonLd,
    loading: boolean
    trusted?: boolean,
    error?: Error,
}
// I could probably make this better if I rewrote the jsig type declarations...
export const getSignerUrl = (vc: VerifiableClaim) =>
    (<jsonld.JsonLd>(<jsonld.JsonLd>vc.data.proof)['dc:creator']).id as string

export const claims: {[url: string]: VerifiableClaim} = {}

export const load = async (url: string) => {
    if (claims[url] !== undefined) {
        return claims[url]
    }
    try {
        const claim: VerifiableClaim = claims[url] = {
            data: {},
            trusted: undefined,
            loading: true
        }
        /// IMPORTANT! insert the data and mark it loading before we do any awaits,
        /// to prevent races.

        const data: jsonld.JsonLd = await m.request<jsonld.JsonLd>({
            method: 'GET',
            url,
        })
        claim.data = data

        if (url === 'http://localhost:8080/data/entity/acr' || url === 'http://localhost:8080/data/entity/acr/keys/1') {
            // autotrust ACR
            claim.trusted = true
            claim.loading = false
            return claim
        }

        if (data.proof) {
            console.log('verifying', url)
            const signerUrl = getSignerUrl(claim)

            const signer = await load(signerUrl)
            if (signer.trusted === undefined) {
                console.error('signer of', url, 'unknown trust, which should never happen???')
                claim.trusted = false
                claim.loading = false
                return claim
            }
            if (signer.trusted === false) {
                console.error('signer of', url, 'not trusted, not verifying')
                claim.trusted = false
                claim.loading = false
                return claim
            }
            // signer is trusted!
            // let's check the keys.
            const publicKeyUrl = <string>signer.data.publicKey
            const publicKey = await load(publicKeyUrl)
            if (publicKey.trusted === false) {
                console.error('signer public key not trusted!')
            }

            claim.trusted = await jsig.verify(claim.data, {
                publicKeyOwner: signer.data,
                publicKey: publicKey.data
            })

            console.log('trusted?', url, claim.trusted)
        }
        claims[url].loading = false
        return claims[url]
    } catch (e) {
        console.error(`failed to load jsonld from ${url}:`, e)
        claims[url] = {
            data: {},
            loading: false,
            trusted: false,
            error: e
        }
        claims[url].loading = false
        return claims[url]
    }
}