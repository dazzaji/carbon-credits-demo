import { Page, tabs, icons } from '../util'
import m from 'mithril'

interface Attrs {
    page: Page
}

export default {
    view(v) {
        const tiles = tabs.map(p => {
            if (p === 'spacer') {
                return m('div', {style: 'flex-grow: 1;'})
            }

            let clazz = 'icon-container'
            if (p === v.attrs.page) {
                clazz += ' is-'+p
            }
            if (p === 'settings') {
                clazz += ''
            }

            return m('div', {class: clazz},
                    m('a', {href: `#!/${p}`, class: `icon big-icon is-${p}`},
                        m('i', {class: `fas ${icons[p]} fa-4x`})
                    )
                )
        })

        return m('div', {id: 'columns', class: 'columns'}, [
            m('div', {class: 'column is-2', style: 'flex-direction: column; display: flex; background-color: lightgray;'},
                tiles
            ),
            m('div', {class: `column is-1 is-${v.attrs.page}`}),
            m('div', {class: 'column'}, v.children)
        ])
    }
} as m.Component<Attrs>
