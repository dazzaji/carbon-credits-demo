import m from 'mithril'
import * as LinkedData from '../models/linked-data'

type KnownField = {
    icon: string,
    source: string
}

const knownFields: {[field: string]: KnownField} = {
    name: {
        icon: 'fa-user',
        source: 'https://schema.org/name'
    },
    legalName: {
        icon: 'fa-building',
        source: 'https://schema.org/legalName'
    }
}

interface Attrs {
    url: string,
}

interface State {
    submenu: undefined | 'trust' | 'raw'
}

const SPECIAL_KEYS = ['@id', '@context', 'proof']

export default {
    oninit: async (v) => {
        const q = await LinkedData.load(v.attrs.url)
        //m.redraw()
    },
    view: (v) => {
        const claim = LinkedData.claims[v.attrs.url]
        if (claim.loading) {
            return 'gimme a sec'
        }
        const keys = Object.keys(claim.data)
        keys.sort()

        const tags = keys
            .filter(f => SPECIAL_KEYS.indexOf(f) === -1)
            .map(k => m('span', {class: 'tag is-dark'}, k))
        
        return m('div', {class: 'tile is-2 box'}, tags)
    }
} as m.Component<Attrs, State>