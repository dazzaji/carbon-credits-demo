import * as nedb from 'nedb'
import { promisify } from 'util'
import argv from './argv'
import log from './log'
import { resolve } from 'url'


log.info("loading db")

export const db = new nedb({ filename: argv.dbfile, autoload: true })
db.ensureIndex({
    fieldName: '@id',
    unique: true,
    sparse: true
})
db.ensureIndex({
    fieldName: 'privateKeyOwner',
    unique: true,
    sparse: true
})

export const insert = (doc: any): Promise<any> =>
    new Promise((res, rej) => db.insert(doc, (err, out) => {
        if (err) rej(err)
        else res(out)
    }))

// ok look YOU try to indent this
export const update =
    (query: any, updateQuery: any, options?: nedb.UpdateOptions):
        Promise<{numberOfUpdated: number, upsert: boolean}> =>
    new Promise((res, rej) => db.update(query, updateQuery, options, (err, numberOfUpdated, upsert) => {
        if (err) rej(err)
        else res({ numberOfUpdated, upsert })
    }))

export const findOne = (query: any, projection?: any): Promise<any> => 
    new Promise((res, rej) => db.findOne(query, projection || {}, (err, doc) => {
        if (err) rej(err)
        else res(doc)
    }))

export const find = (query: any, projection?: any): Promise<any[]> => 
    new Promise((res, rej) => db.find(query, projection || {}, (err, docs) => {
        if (err) rej(err)
        else res(docs)
    }))


export const getDataUrl = (extra: string) => new URL(extra, `http://${argv.domain}:${argv.port}/`).toString()