import * as yargs from 'yargs'
import log from './log'

const argv = yargs
    .env('VC_DEMO')
    .option('dbfile', {
        alias: 'f',
        default: './vc-demo.nedb',
        describe: 'the file to persist the database to'
    })
    .option('port', {
        alias: 'p',
        default: 8080,
        describe: 'the port to serve on'
    })
    .option('domain', {
        alias: 'h',
        default: 'localhost',
        describe: 'the domain to anchor @ids at'
    })
    .argv

export default argv