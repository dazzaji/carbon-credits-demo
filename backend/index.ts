import * as express from 'express'
import { RequestHandler } from 'express'
import * as jsonld from 'jsonld'
import * as jsig from 'jsonld-signatures'
import { URL } from 'url'
import * as path from 'path'
import * as crypto from 'crypto'
import * as _ from 'lodash'
import * as assert from 'assert'

import log from './log'
import argv from './argv'
import * as db from './db'
import { sign } from './security'
import { insertInitialEntities } from './populate'

log.info('startup')

log.info('load jsig')
jsig.use('jsonld', jsonld)

log.info('inserting test data')

log.info('cfg router')
const app = express()

// json-ld
app.route('/data*').get(async (req, res) => {
    log.info(`get data: ${req.path}`)
    const id = db.getDataUrl(req.path)
    const data = await db.findOne({ '@id': id }, {})
    delete data._id
    res.type('application/json')
    res.send(data)
})

// assets
const dist = path.join(path.dirname(__dirname), 'dist')

// index
app.get('/', (req, res) => {
    res.sendFile(path.join(dist, 'index.html'))
})
app.use('/static', express.static(dist))

app.use(express.json())

// catch errors in our async handlers
const asyncHandler: (q: RequestHandler) => RequestHandler = fn => (req, res, next) => {
    Promise.resolve(fn(req, res, next))
      .catch(next)
}

// usage: POST /api/sign { "who": }
app.route('/api/sign').post(asyncHandler(async (req, res) => {
    assert(req.body)
    assert.strictEqual(typeof req.body.who, 'string')

    res.send({
        success: await sign(req.body.who, req.body.what)
    })
}));

log.info(`listening forever on ${argv.port}...`);

(async () => {
    await insertInitialEntities()
    app.listen(argv.port)
})()

