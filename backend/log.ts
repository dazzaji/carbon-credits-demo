import { createLogger, format, transports } from "winston"

const log = createLogger({
    format: format.combine(
        format.timestamp({format: 'YY-MM-DD HH:mm:ss'}),
        format.colorize(),
        format.printf((info: any) => `${info.timestamp} [${info.level}] ${info.message}`),
    ),
    transports: [
        new transports.Console(),
    ]
})

export default log