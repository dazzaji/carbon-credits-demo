/// <reference path='../shared/jsonld.d.ts'/>

import * as db from './db'
import * as assert from 'assert'
import * as jsig from 'jsonld-signatures'

export const sign = async (who: string, what: any, key?: string) => {
    if (!key) {
        const keyDoc = await db.findOne({ privateKeyOwner: who })
        assert(keyDoc, `no key found for ${who}`)
        key = keyDoc.privateKey
    }

    return await jsig.sign(what, {
        algorithm: 'RsaSignature2018',
        privateKeyPem: key,
        creator: who
    })
}
